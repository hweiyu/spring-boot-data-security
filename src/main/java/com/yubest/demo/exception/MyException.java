package com.yubest.demo.exception;

/**
 * @Author hweiyu
 * @Description
 * @Date 2021/7/6 14:59
 */
public class MyException extends RuntimeException {

   public MyException(String message) {
       super(message);
   }

}
