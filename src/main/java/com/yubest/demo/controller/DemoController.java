package com.yubest.demo.controller;

import com.yubest.demo.dto.DemoRespDTO;
import com.yubest.demo.dto.Response;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author hweiyu
 * @Description
 * @Date 2021/3/1 14:01
 */
@RestController
public class DemoController {

    /**
     * @return
     */
    @GetMapping(value = "/test")
    public Response<DemoRespDTO> test() {
        DemoRespDTO respDTO = new DemoRespDTO();
        respDTO.setUserName("张三");
        respDTO.setPhone("18812345678");
        respDTO.setIdCard("15030319520807064X");
        respDTO.setPassword("asdf12345678");
        respDTO.setCustomValue("sfwegewgrergergwefwefwef");
        return Response.success(respDTO);
    }

}
